const timerHours = document.querySelector(".hours");
const timerMinutes = document.querySelector(".minutes");
const timerSeconds = document.querySelector(".seconds");

// seconds.textContent = 'hello'

let time = new Date();
let countDownDate = new Date("Jan 30, 2024 9:2:30").getTime();
//  countDownDate

var x = setInterval(function () {
  // Get today's date and time
  const now = new Date().getTime();

  // Find the distance between now and the count down date
  var distance = countDownDate - now;

  // Time calculations for days, hours, minutes and seconds
  // var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
  timerHours.textContent = hours;
  timerMinutes.textContent = minutes;
  timerSeconds.textContent = seconds;

  // Display the result in the element with id="demo"
  // document.getElementById("demo").innerHTML =
  // days + "d " + hours + "h " + minutes + "m " + seconds + "s ";

  // If the count down is finished, write some text
  if (distance < 0) {
    clearInterval(x);
    document.getElementById("demo").innerHTML = "EXPIRED";
  }
}, 1000);
